<?php
if (!extension_loaded('mysqli')){
	echo "La extension no esta habilitada";
	exit;
}
error_reporting(E_ALL);
ini_set('display_errors',1);

// Datos de conexión a la base de datos
$servername = "127.0.0.1";
$username = "nandito";
$password = "Nandonando#3527";
$database = "Datos";

// Crear conexión
$conn = new mysqli($servername, $username, $password, $database);

// Verificar conexión
if ($conn->connect_error) {
    $response = array("error" => true, "message" => "Error de conexión: " . $conn->connect_error);
    echo json_encode($response);
    exit;
}

// Obtener los datos enviados por el cliente
$nombreJugador1 = $_POST['nombreJugador1'];
$nombreJugador2 = $_POST['nombreJugador2'];
$victoriasJugador1 = $_POST['victoriasJugador1'];
$derrotasJugador1 = $_POST['derrotasJugador1'];
$victoriasJugador2 = $_POST['victoriasJugador2'];
$derrotasJugador2 = $_POST['derrotasJugador2'];

// Sanitizar los datos para prevenir inyecciones SQL
$nombreJugador1 = $conn->real_escape_string($nombreJugador1);
$nombreJugador2 = $conn->real_escape_string($nombreJugador2);

// Consulta SQL para insertar los datos
$sql = "INSERT INTO datos (nombre, victorias, derrotas) VALUES ('$nombreJugador1', $victoriasJugador1, $derrotasJugador1), ('$nombreJugador2', $victoriasJugador2, $derrotasJugador2)";

if ($conn->query($sql) === TRUE) {
    $response = array("error" => false, "message" => "Datos guardados correctamente");
} else {
    $response = array("error" => true, "message" => "Error al guardar los datos: " . $conn->error);
}


// Cerrar conexión
$conn->close();

// Enviar respuesta al cliente
echo json_encode($response);
?>
